const constants = require('../pageobjects/constants');
const helpers = require('../pageobjects/helpers');


describe("Calc test", () => {   

    it("Sum", () => {
        browser.initSession(browser.baseUrl);

        browser.checkResult('0');
        browser.waitAndClick("7");
        browser.checkResult('7');

        browser.waitAndClick("+");
        browser.checkResult('7');

        browser.waitAndClick("5");
        browser.checkResult('5');

        browser.waitAndClick("=");
        browser.checkResult('14');
    });

    it("Minus", () => {
        browser.initSession(browser.baseUrl);
        browser.waitAndClick("1");
        browser.waitAndClick("7");
        browser.checkResult('17');

        browser.waitAndClick("-");
        browser.checkResult('17');

        browser.waitAndClick("7");
        browser.checkResult('7');

        browser.waitAndClick("=");
        browser.checkResult('10'); 
    });

    it("AC", () => {
        browser.initSession(browser.baseUrl);
        browser.waitAndClick("1");
        browser.waitAndClick("7");
        browser.checkResult('17');

        browser.waitAndClick("AC");
        browser.checkResult('0');
    });

    it("+/-", () => {
        browser.initSession(browser.baseUrl);
        browser.waitAndClick("1");
        browser.waitAndClick("+/-");
        browser.checkResult('-1');
    });

});
   
